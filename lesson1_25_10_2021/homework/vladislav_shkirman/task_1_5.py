# Right triangle is given along with legs
# Need to find hypotenuse and area of a triangle

a = 6
b = 8
hypotenuse = int((a**2 + b**2)**0.5)
print("Hypotenuse of a right triangle with legs", a, "and", b, "is", hypotenuse)
triangle_area = int((a*b)/2)
print("Area of a right triangle with legs", a, "and", b, "is", triangle_area, "square meters")
